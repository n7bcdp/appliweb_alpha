package compte;

import java.util.ArrayList;
import java.util.Collection;
import voyage.*;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import voyage.Voyage;

@Entity
public class Utilisateur {

	
	private String prenom;
	private String nom;
	private String pays;
	


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUtilisateur;
	



	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Voyage> voyages;
	
	
	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Utilisateur> amis;
	
	@OneToOne(fetch=FetchType.EAGER)
	private transient CompteUtilisateur compte;	
	
	public Utilisateur() {}
	
	
	public Utilisateur(String prenom, String nom, String pays) {
		this.prenom = prenom;
		this.nom = nom;
		this.pays = pays;
		voyages = new ArrayList<Voyage>();
		this.amis = new ArrayList<Utilisateur>();
	}

	
	
	public void ajouterCompte (CompteUtilisateur c) {
		this.compte=c;
	}
	
	public void ajouterVoyage (Voyage v) {
		this.voyages.add(v);
	}
	
	
	public String getPays() {
		return pays;
	}


	public void setPays(String pays) {
		this.pays = pays;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}


	public Collection<Utilisateur> getAmis() {
		return amis;
	}


	public CompteUtilisateur getCompte() {
		return compte;
	}

	
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public Collection<Voyage> getVoyages() {
		return voyages;
	}


	public void setVoyages(Collection<Voyage> voyages) {
		this.voyages = voyages;
	}


	
	
}
