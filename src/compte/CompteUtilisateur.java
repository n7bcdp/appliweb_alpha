package compte;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CompteUtilisateur {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCompte;
	
	@OneToOne(fetch=FetchType.EAGER)
	private Utilisateur owner;
	
	

	private String telephone;
	private String adresseMail;
	

	private String motdepasse;


	
	public CompteUtilisateur () {}
	
	public CompteUtilisateur(String telephone, String adresseMail, String motdepasse) {
		this.telephone = telephone;
		this.adresseMail = adresseMail;
		this.motdepasse = motdepasse;
	}
	
	public CompteUtilisateur(String adresseMail, String motdepasse) {
		this.adresseMail = adresseMail;
		this.motdepasse = motdepasse;
	}
	
	
	public Utilisateur getOwner() {
		return owner;
	}

	public void setOwner(Utilisateur owner) {
		this.owner = owner;
	}

	
	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getAdresseMail() {
		return adresseMail;
	}


	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}


	public String getMotdepasse() {
		return motdepasse;
	}


	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}

	public int getIdCompte() {
		return idCompte;
	}




	
	
	
	
}
