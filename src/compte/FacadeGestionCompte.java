package compte;

import java.util.Collection;

import javax.ejb.SessionBean;
import javax.ejb.Singleton;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.security.auth.login.Configuration;

@Singleton
public class FacadeGestionCompte {

	
	private static final String[] alphabet = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	private static final Integer[] listecles= {1,2,3,5,8,10,1,4,5,21,12,13,7,9,1,0,2};
	
	@PersistenceContext
	EntityManager em;
	
	
	public FacadeGestionCompte () {}
	
	
	
	public Utilisateur ajoutUtilisateur(String prenom,String nom,String pays) {
		Utilisateur utilisateur = new Utilisateur(prenom, nom,pays);		
		em.persist(utilisateur);
		return utilisateur;
	}
	
	
	
	public CompteUtilisateur ajoutCompteUtilisateur(String telephone,String adresseMail,String mdp) {
		CompteUtilisateur compteutilisateur = new CompteUtilisateur(telephone,adresseMail,mdp);
		em.persist(compteutilisateur);
		return compteutilisateur;
		
	}
	
	public void associer(int idCompte, int idUser) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		CompteUtilisateur compteUser = em.find(CompteUtilisateur.class, idCompte);
		user.ajouterCompte(compteUser);
		compteUser.setOwner(user);
		
	}
	
	
	public CompteUtilisateur getCompteUtilisateurByProperties (String adresseMail,String mdp) {
		TypedQuery<CompteUtilisateur> req = (TypedQuery<CompteUtilisateur>) em.createQuery("select cu from CompteUtilisateur cu where cu.adresseMail = :AdresseMail and cu.motdepasse = :mdp ");
		req.setParameter("AdresseMail",adresseMail);
		req.setParameter("mdp", mdp);
		return req.getSingleResult();
	}
	
	
	public Utilisateur getUtilisateurbyCompteutilisateur (CompteUtilisateur cu) {
		return cu.getOwner();
	}
	
	public Utilisateur getUtilisateurbyId ( int idUser) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		return user;
		
	}
	
	
	public Collection<Utilisateur> listerUtilisateur() {
		TypedQuery<Utilisateur> req = (TypedQuery<Utilisateur>) em.createQuery("select u from Utilisateur u", Utilisateur.class);
		return req.getResultList();
	}
	
	
	
	
	public void modifierNom(int idUser,String nouveaunom) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		user.setNom(nouveaunom);
	}
	
	public void modifierPrenom(int idUser,String nouveauprenom) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		user.setPrenom(nouveauprenom);
	}
	
	public void modifierPays(int idUser,String nouveauPays) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		user.setPays(nouveauPays);
	}
	
	public void modifierAdresse(int idCompteUser,String nouvelleAdresse) {
		CompteUtilisateur CompteUser = em.find(CompteUtilisateur.class, idCompteUser);
		CompteUser.setAdresseMail(nouvelleAdresse);
	}
	
	public void modifierPassword(int idCompteUser,String nouveaumdp) {
		CompteUtilisateur CompteUser = em.find(CompteUtilisateur.class, idCompteUser);
		CompteUser.setMotdepasse(nouveaumdp);
	}
	
	
	public String getNom(int idUser) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		return user.getNom();
	}
	
	public String getPrenom(int idUser) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		return user.getPrenom();
	}
	
	public String getPays(int idUser) {
		Utilisateur user = em.find(Utilisateur.class, idUser);
		return user.getPays();
	}
	
	public String getAdresseMail(int idCompteUser) {
		CompteUtilisateur compteUser = em.find(CompteUtilisateur.class, idCompteUser);
		return compteUser.getAdresseMail();
	}
	
	
	public String getPassword(int idCompteUser) {
		CompteUtilisateur compteUser = em.find(CompteUtilisateur.class, idCompteUser);
		return compteUser.getMotdepasse();
	}
	
	
	public boolean connexionUtilisateur(String AdresseMail, String mdp) {
		TypedQuery<CompteUtilisateur> req = (TypedQuery<CompteUtilisateur>) em.createQuery("select cu from CompteUtilisateur cu where cu.adresseMail = :AdresseMail and cu.motdepasse = :mdp ");
		req.setParameter("AdresseMail",AdresseMail);
		req.setParameter("mdp", mdp);
		if (req.getResultList().size()!=1) {
			return false;
		}
		else {
			return true;
		}
	}
	
	
	public boolean mailValide(String adresseMail) {
		String [] separation = adresseMail.split("@");
		if (separation.length==2) {
			if(separation[0].length()>0 && separation[1].length()>0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		
	}
	
	/*
	public void chiffrer(CompteUtilisateur compte) {
		int length = compte.getMotdepasse().length();
		String res = null;
		int indexintermediaire;
		for (int i=0;i<length;i++) {
			 indexintermediaire = indexOf(compte.getMotdepasse().valueOf(i)) + listecles[i];
			 System.out.println(indexintermediaire);
			 indexintermediaire = indexintermediaire%26;
			 res = res + alphabet[indexintermediaire];
		}
		compte.setMotdepasse(res);
		
	}
	
	
	public void dechiffrer(CompteUtilisateur compte) {
		int length = compte.getMotdepasse().length();
		String res = null;
		int indexintermediaire;
		for (int i=0;i<length;i++) {
			 indexintermediaire = indexOf(compte.getMotdepasse().valueOf(i)) - listecles[i];
			 if (indexintermediaire<0) {
				 indexintermediaire=indexintermediaire+26;
			 }
			 res = res + alphabet[indexintermediaire];
		}
		compte.setMotdepasse(res);
			
	}
	
	*/
	/*Fonction auxiliaire pour chiffrer/d�chiffrer*/
	public static int indexOf(String s) {
		int compteur=0;
		System.out.println(s);
		while(!(alphabet[compteur].equals(s))) {
			compteur++;
		}
		return compteur;
	}
	
	
	
	
	public boolean passwordValide(String password) {
		if (4<=password.length() && password.length()<=12) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void ajouterAmi(int idUser1, int idUser2) {
		Utilisateur u1 = em.find(Utilisateur.class, idUser1);
		Utilisateur u2 = em.find(Utilisateur.class, idUser2);
		if (!u1.getAmis().contains(u2)) {
			u1.getAmis().add(u2);
			u2.getAmis().add(u1);
		}

	}
	
	public void supprimerAmi(int idUser1, int idUser2) {
		Utilisateur u1 = em.find(Utilisateur.class, idUser1);
		Utilisateur u2 = em.find(Utilisateur.class, idUser2);
		u1.getAmis().remove(u2);
		u2.getAmis().remove(u1);
	}
	
	public Collection<Utilisateur> listerAmis(int idUser){
		Utilisateur u = em.find(Utilisateur.class, idUser);
		return u.getAmis();
	}
	
	
	
	
	
}
