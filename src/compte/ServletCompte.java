package compte;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import outils.ExclusionStrategyImpl;

/**
 * Servlet implementation class ServletCompte
 */
@WebServlet("/ServletCompte")
public class ServletCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String ATT_SESSION_USER = "sessionUtilisateur";
    
	@EJB
	FacadeGestionCompte facade;

    public ServletCompte() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getParameter("op");
		String redirection = (String) request.getAttribute("op");
		HttpSession session = null;
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategyImpl()).create();
		
		if (redirection == "versModification") {
			operation = "versModification";
		}
		switch(operation) {
		
		case "connexion" :{
			String adresseMail = request.getParameter("registerEMail");
			String password = request.getParameter("registerPassword");
			if (facade.connexionUtilisateur(adresseMail,password)) {
				session = request.getSession();
				//Recup�rer le compteUtilisateur pour le donner � la session
				CompteUtilisateur CompteUser = facade.getCompteUtilisateurByProperties(adresseMail, password);
				session.setAttribute(ATT_SESSION_USER,CompteUser);
				String s = "connected";
				session.setAttribute("test", s);
				response.sendRedirect("#/voyages");
				//request.getRequestDispatcher("index.html").forward(request,response);
			}
			else {
				String s = "Le mot de passe rentré ne correspond pas à l'adresse mail";
				request.setAttribute("connexionInvalide", s);
				request.getRequestDispatcher("connexion.jsp").forward(request,response);
			}
			break;
			
		}
		
		case "inscription" : {
			String adresseMail = request.getParameter("registerEMail");
			String password = request.getParameter("registerPassword");
			String passwordValidation = request.getParameter("registerPasswordValidation");
			String nom = request.getParameter("registerFirstName");
			String prenom = request.getParameter("registerLastName");
			String pays = request.getParameter("registerCountry");
			String telephone = request.getParameter("registerPhone");
			
			if (facade.mailValide(adresseMail)) {
				if (facade.passwordValide(password)) {
					if (password.equals(passwordValidation)) {
						Utilisateur utilisateur = facade.ajoutUtilisateur(prenom, nom, pays);
						CompteUtilisateur compteUtilisateur = facade.ajoutCompteUtilisateur(telephone, adresseMail, password);
						facade.associer(compteUtilisateur.getIdCompte(), utilisateur.getIdUtilisateur());
						request.getRequestDispatcher("ServletCompte?op=connexion&registerEMail="+adresseMail+"&registerPassword="+password).forward(request, response);
						//request.getRequestDispatcher("index.html").forward(request, response);
					}
					else {
						String s = "Les 2 mots de passe ne sont pas identiques";
						System.out.print(s);
						request.setAttribute("mdpNonIdentique", s);
						request.getRequestDispatcher("inscription.jsp").forward(request, response);
					}
				}
				else {
					
					String s = "Le mot de passe doit contenir entre 4 et 12 caractères ";
					System.out.print(s);
					request.setAttribute("mdpProbleme", s);
					request.getRequestDispatcher("inscription.jsp").forward(request, response);
				}
			}
			else {
				String s = "L'adresse mail n'est pas valide";
				request.setAttribute("mailInvalide", s);
				request.getRequestDispatcher("inscription.jsp").forward(request, response);
			}
			break;
		}
		
		case "versModification" : {
			session = request.getSession();
			if (session.getAttribute("test") != null) {
			CompteUtilisateur CompteUser = (CompteUtilisateur) session.getAttribute(ATT_SESSION_USER);
			int idCompteUser = CompteUser.getIdCompte();
			Utilisateur User = facade.getUtilisateurbyCompteutilisateur(CompteUser);
			int idUser = User.getIdUtilisateur();
			request.setAttribute("modifPrenom", facade.getPrenom(idUser));
			request.setAttribute("modifNom", facade.getNom(idUser));
			request.setAttribute("modifPays", facade.getPays(idUser));
			request.setAttribute("modifPassword", facade.getPassword(idCompteUser));
			request.setAttribute("modifAdresse", facade.getAdresseMail(idCompteUser));
			request.getRequestDispatcher("gestioncompte.jsp").forward(request, response);
			}
			else {
				response.sendRedirect("/appliWebAlpha/inscription.jsp");
			}
			break;
		}

		case "Modification" : {
			String idUserString =  request.getParameter("idUser");
			int idUser = Integer.parseInt(idUserString);
			int idCompteUser = idUser;
			String adresseMail = request.getParameter("modifadresse");
			String password = request.getParameter("modifpassword");
			String nom = request.getParameter("modifnom");
			String prenom = request.getParameter("modifprenom");
			String pays = request.getParameter("modifpays");
			facade.modifierNom(idUser, nom);
			facade.modifierPrenom(idUser, prenom);
			facade.modifierPays(idUser, pays);
			facade.modifierAdresse(idCompteUser, adresseMail);
			facade.modifierPassword(idCompteUser, password);
			String versModification = "versModification";
			request.setAttribute("op", versModification);
			doGet(request,response);
			break;
		}
		
		case "Deconnexion" : {
			session = request.getSession();
			session.invalidate();
			request.getRequestDispatcher("inscription.jsp").forward(request,response);
			break;
		}

		case "listerUtilisateurs" :  {
			String jsonListeUsers = gson.toJson(facade.listerUtilisateur());
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonListeUsers);
			break;
		}
		
		case "RecupererInfos" : {
			session = request.getSession();
			CompteUtilisateur CUser = (CompteUtilisateur) session.getAttribute(ATT_SESSION_USER);
			int id = CUser.getOwner().getIdUtilisateur();
			String jsonListeInfos = gson.toJson(facade.getUtilisateurbyId(id));
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonListeInfos);
			break;
			
		}
		
		case "RecupererInfosEtranger" : {
			
			String idEtranger = (String) request.getParameter("idUser");
			int id = Integer.parseInt(idEtranger);
			String jsonListeInfos = gson.toJson(facade.getUtilisateurbyId(id));
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonListeInfos);
			break;
		
		}
		
		case "ajouterAmi" : {
			session = request.getSession();
			String idami = request.getParameter("idAmi");
			Integer idAmi = Integer.parseInt(idami);
			CompteUtilisateur CUser = (CompteUtilisateur) session.getAttribute(ATT_SESSION_USER);
			facade.ajouterAmi(CUser.getOwner().getIdUtilisateur(), idAmi);
			response.sendRedirect("utilisateurs.html");
			break;
		}
		
		case "listerAmis" : {
			session = request.getSession();
			CompteUtilisateur CUser = (CompteUtilisateur) session.getAttribute(ATT_SESSION_USER);
			String jsonListeAmis = gson.toJson(facade.listerAmis(CUser.getIdCompte()));
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonListeAmis);	
			break;
		}
		
		case "supprimerAmi":{
			session = request.getSession();
			String idami = request.getParameter("idAmi");
			Integer idAmi = Integer.parseInt(idami);
			CompteUtilisateur CUser = (CompteUtilisateur) session.getAttribute(ATT_SESSION_USER);
			facade.supprimerAmi(CUser.getOwner().getIdUtilisateur(), idAmi);
			response.sendRedirect("#/friends/");
			break;
		}
		
		case "estConnecte" : 
			session = request.getSession();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			if (session.getAttribute(ATT_SESSION_USER) != null) {
				response.getWriter().write("{\"statut\" : \"connecte\"}");
			} else {
				response.getWriter().write("{\"statut\" : \"nonConnecte\"}");
			}
			break;
		default : {
			response.sendRedirect("#/erreur/pageinexistante");
		}
		
		}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
