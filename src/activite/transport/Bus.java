package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Bus. */
@Entity
@DiscriminatorValue("BUS")
public class Bus extends Transport {

	public Bus() {
		this.setType("Bus");
		this.setPicturePath("style/img/activities/bus.png");
	}
	
}
