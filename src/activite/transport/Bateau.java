package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Bateau. */
@Entity
@DiscriminatorValue("BATEAU")
public class Bateau extends Transport {

	public Bateau() {
		this.setType("Bateau");
		this.setPicturePath("style/img/activities/boat.png");
	}
	
}
