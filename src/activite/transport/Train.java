package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Train. */
@Entity
@DiscriminatorValue("TRAIN")
public class Train extends Transport {

	public Train() {
		this.setType("Train");
		this.setPicturePath("style/img/activities/train.png");
	}
	
}
