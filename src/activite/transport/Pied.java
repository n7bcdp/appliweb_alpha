package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Pied. */
@Entity
@DiscriminatorValue("PIED")
public class Pied extends Transport {
	
	public Pied() {
		this.setType("A pied");
		this.setPicturePath("style/img/activities/walker.png");
	}
	
}