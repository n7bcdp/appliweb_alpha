package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Métro. */
@Entity
@DiscriminatorValue("METRO")
public class Metro extends Transport {

	public Metro() {
		this.setType("Métro");
		this.setPicturePath("style/img/activities/subway.png");
	}
	
}
