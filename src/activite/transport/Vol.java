package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Vol. */
@Entity
@DiscriminatorValue("VOL")
public class Vol extends Transport {

	public Vol() {
		this.setType("Vol");
		this.setPicturePath("style/img/activities/flight.png");
	}
	
}
