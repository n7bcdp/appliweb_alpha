package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Voiture. */
@Entity
@DiscriminatorValue("VOITURE")
public class Voiture extends Transport {

	public Voiture() {
		this.setType("Voiture");
		this.setPicturePath("style/img/activities/car.png");
	}
	
}