package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Taxi. */
@Entity
@DiscriminatorValue("TAXI")
public class Taxi extends Transport {

	public Taxi() {
		this.setType("Taxi");
		this.setPicturePath("style/img/activities/taxi.png");
	}
	
}
