package activite.transport;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Transport;

/** Classe pour un transport de type Covoiturage. */
@Entity
@DiscriminatorValue("COVOIT")
public class Covoiturage extends Transport {

	public Covoiturage() {
		this.setType("Covoiturage");
		this.setPicturePath("style/img/activities/carpooling.png");
	}
	
}