package activite;

import java.io.File;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/** Super classe Activite, contient toutes les informations globales des différentes activités. */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("ACTIVITE")
public class Activite {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	/** L'identifiant de l'activité. */
	private Integer id;

	/** Le nom de l'activité. */
	private String nom;

	/** La date de début de l'activité. */
	private String dateDebut;

	/** La date de fin de l'activité. */
	private String dateFin;

	/** L'heure de début de l'activité. */
	private String heureDebut;

	/** L'heure de fin de l'activité. */
	private String heureFin;

	/** Le lieu de début de l'activité. */
	private String nomLieuDebut;
	
	/** La latitude du lieu. */
	private String latitudeLieu;

	/** La latitude du lieu. */
	private String longitudeLieu;

	/** Le lieu de fin de l'activité. */
	private String nomLieuFin;

	/** La latitude du lieu de fin (si déplacement). */
	private String latitudeFin;

	/** La longitude du lieu de fin (si déplacement). */
	private String longitudeFin;
	
	/** Le type de l'activité. */
	private String type;

	/** Le prix de l'activité. */
	private int prix;
	
	/** Le numéro de téléphone du lieu d'hébergement. */
	private String numeroTelephone;
	
	/** Le fichier PDF de l'activité, réservation, billet, etc. */
	private File fichierPDF;
	
	/** La description de l'activité. */
	private String description;
	
	/** Le lien internet de l'activité. */
	private String lienInternet;
	
	/** Le path vers l'image de l'activité. */
	private String picturePath;

	
	public Activite() {
		this.setPicturePath("style/img/activities/defaut.png");
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/** Obtenir le nom.
	 * @return le nom
	 */
	public String getNom() {
		return nom;
	}

	/** Fixer le nom.
	 * @param _nom le nouveau nom
	 */
	public void setNom(String _nom) {
		this.nom = _nom;
	}

	/** Obtenir la date de début.
	 * @return la date de début
	 */
	public String getDateDebut() {
		return dateDebut;
	}

	/** Fixer la nouvelle date de début.
	 * @param _dateDebut la nouvelle date de début
	 */
	public void setDateDebut(String _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/** Obtenir la date de fin.
	 * @return la dateFin
	 */
	public String getDateFin() {
		return dateFin;
	}

	/** Fixer la nouvelle date de fin.
	 * @param _dateFin la nouvelle date de fin
	 */
	public void setDateFin(String _dateFin) {
		this.dateFin = _dateFin;
	}

	/** Obtenir l'heure de début.
	 * @return l'heure de début
	 */
	public String getHeureDebut() {
		return heureDebut;
	}

	/** Fixer la nouvelle heure de début.
	 * @param _heureDebut la nouvelle heure de début
	 */
	public void setHeureDebut(String _heureDebut) {
		this.heureDebut = _heureDebut;
	}

	/** Obtenir l'heure de fin.
	 * @return l'heure de fin
	 */
	public String getHeureFin() {
		return heureFin;
	}

	/** Fixer la nouvelle heure de fin.
	 * @param _heureFin la nouvelle heure de fin
	 */
	public void setHeureFin(String _heureFin) {
		this.heureFin = _heureFin;
	}

	/** Obtenir le lieu de début.
	 * @return le lieuDebut
	 */
	public String getNomLieuDebut() {
		return nomLieuDebut;
	}

	/** Fixer le nouveau lieu de début.
	 * @param _lieuDebut le nouveau lieu de début
	 */
	public void setNomLieuDebut(String _lieuDebut) {
		this.nomLieuDebut = _lieuDebut;
	}

	/** Obtenir la latitude du lieu.
	 * @return la latitude du lieu
	 */
	public String getLatitudeLieu() {
		return latitudeLieu;
	}

	/** Fixer la nouvelle latitude.
	 * @param _latitudeLieu la nouvelle latitude du lieu
	 */
	public void setLatitudeLieu(String _latitudeLieu) {
		this.latitudeLieu = _latitudeLieu;
	}

	/** Obtenir la longitude du lieu.
	 * @return le longitude du lieu
	 */
	public String getLongitudeLieu() {
		return longitudeLieu;
	}

	/** Fixer la nouvelle longitude.
	 * @param _longitudeLieu la nouvelle longitude du lieu
	 */
	public void setLongitudeLieu(String _longitudeLieu) {
		this.longitudeLieu = _longitudeLieu;
	}

	/** Obtenir le lieu de fin.
	 * @return le lieu de fin
	 */
	public String getNomLieuFin() {
		return nomLieuFin;
	}

	/** Fixer le nouveau lieu de fin.
	 * @param _lieuFin le nouveau lieu de fin
	 */
	public void setNomLieuFin(String _lieuFin) {
		this.nomLieuFin = _lieuFin;
	}

	/** Obtenir la latitude de fin.
	 * @return la latitude de fin
	 */
	public String getLatitudeFin() {
		return latitudeFin;
	}

	/** Fixer la nouvelle latitude de fin.
	 * @param _latitudeFin la nouvelle latitude de fin
	 */
	public void setLatitudeFin(String _latitudeFin) {
		this.latitudeFin = _latitudeFin;
	}

	/** Obtenir la longitude de fin.
	 * @return la longitude de fin
	 */
	public String getLongitudeFin() {
		return longitudeFin;
	}

	/** Fixer la nouvelle longitude de fin.
	 * @param _longitudeFin la nouvelle longitude de fin
	 */
	public void setLongitudeFin(String _longitudeFin) {
		this.longitudeFin = _longitudeFin;
	}

	/** Obtenir le type.
	 * @return le type
	 */
	public String getType() {
		return type;
	}

	/** Fixer le nouveau type.
	 * @param _type le nouveau type
	 */
	public void setType(String _type) {
		this.type = _type;
	}

	/** Obtenir le prix
	 * @return le prix
	 */
	public int getPrix() {
		return prix;
	}

	/** Fixer le nouveau prix
	 * @param _prix le nouveau prix
	 */
	public void setPrix(int _prix) {
		this.prix = _prix;
	}

	/** Obtenir le numéro de téléphone.
	 * @return le numéro de téléphone
	 */
	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	/** Fixer le nouveau numéro de téléphone.
	 * @param _numeroTelephone le nouveau numéro de téléphone
	 */
	public void setNumeroTelephone(String _numeroTelephone) {
		this.numeroTelephone = _numeroTelephone;
	}

	/** Obtenir le fichier PDF.
	 * @return le fichierPDF
	 */
	public File getFichierPDF() {
		return fichierPDF;
	}

	/** Fixer le nouveau fichier PDF.
	 * @param _fichierPDF le nouveau fichier PDF
	 */
	public void setFichierPDF(File _fichierPDF) {
		this.fichierPDF = _fichierPDF;
	}

	/** Obtenir la description.
	 * @return la description
	 */
	public String getDescription() {
		return description;
	}

	/** Fixer la nouvelle description.
	 * @param _description la nouvelle description
	 */
	public void setDescription(String _description) {
		this.description = _description;
	}

	/** Obtenir le lien Internet.
	 * @return le lien Internet
	 */
	public String getLienInternet() {
		return lienInternet;
	}

	/** Fixer le nouveau lien Internet.
	 * @param _lienInternet le nouveau lien Internet
	 */
	public void setLienInternet(String _lienInternet) {
		this.lienInternet = _lienInternet;
	}
	
	/** Obtenir le path de l'image.
	 * @return le path de l'image
	 */
	public String getPicturePath() {
		return picturePath;
	}

	/** Fixer le nouveau path de l'image.
	 * @param _picturePath le nouveau path de l'image
	 */
	public void setPicturePath(String _picturePath) {
		this.picturePath = _picturePath;
	}

}
