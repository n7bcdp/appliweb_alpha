package activite;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/** Super classe d'Hébergement, c'est une activité. */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("HEBERG")
public class Hebergement extends Activite {
	
	/** Le numéro de réservation. */
	private Integer numeroReservation;
	
	/** L'adresse du lieu d'hébergement. */
	private String adresse;

	public Hebergement() {
		super();
	}

	/** Obtenir le numéro de réservation.
	 * @return le numéro de réservation
	 */
	public Integer getNumeroReservation() {
		return numeroReservation;
	}

	/** Fixer le nouveau numéro de réservation.
	 * @param _numeroReservation le nouveau numéro de réservation
	 */
	public void setNumeroReservation(Integer _numeroReservation) {
		this.numeroReservation = _numeroReservation;
	}

	/** Obtenir l'adresse.
	 * @return l'adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/** Fixer la nouvelle adresse .
	 * @param _adresse la nouvelle adresse
	 */
	public void setAdresse(String _adresse) {
		this.adresse = _adresse;
	}

}
