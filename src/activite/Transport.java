package activite;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/** Super classe de Transport, c'est une activité. */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("TRANSPORT")
public class Transport extends Activite {
	
	/** Le numéro de réservation. */
	private String numeroReservation;
	
	/** Le numéro de la place réservée. */
	private String numeroPlace;
	
	/** Le numéro de vol, de bus, ... */
	private String numeroTransport;
	
	/** La compagnie de transport. */
	private String compagnieTransport;
	
	public Transport() {
		super();
	}

	/** Obtenir le numéro de réservation.
	 * @return le numéro de réservation
	 */
	public String getNumeroReservation() {
		return numeroReservation;
	}

	/** Fixer le nouveau numéro de réservation.
	 * @param _numeroReservation le nouveau numéro de réservation
	 */
	public void setNumeroReservation(String _numeroReservation) {
		this.numeroReservation = _numeroReservation;
	}

	/** Obtenir le numéro de place.
	 * @return le numéro de place
	 */
	public String getNumeroPlace() {
		return numeroPlace;
	}

	/** Fixer le nouveau numéro de place.
	 * @param _numeroPlace le nouveau numéro de place
	 */
	public void setNumeroPlace(String _numeroPlace) {
		this.numeroPlace = _numeroPlace;
	}

	/** Obtenir le numéro de transport.
	 * @return le numéro de transport
	 */
	public String getNumeroTransport() {
		return numeroTransport;
	}

	/** Fixer le nouveau numéro de transport.
	 * @param _numeroTransport le nouveau numéro de transport
	 */
	public void setNumeroTransport(String _numeroTransport) {
		this.numeroTransport = _numeroTransport;
	}

	/** Obtenir la compagnie de transport.
	 * @return la compagnie de transport
	 */
	public String getCompagnieTransport() {
		return compagnieTransport;
	}

	/** Fixer la nouvelle compagnie de transport.
	 * @param _compagnieTransport la nouvelle compagnie de transport
	 */
	public void setCompagnieTransport(String _compagnieTransport) {
		this.compagnieTransport = _compagnieTransport;
	}
	
}
