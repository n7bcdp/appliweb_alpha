package activite.sortie;

import activite.Activite;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** Classe pour une activité de type Sport. */
@Entity
@DiscriminatorValue("SPORT")
public class Sport extends Activite {

	public Sport() {
		this.setType("Sport");
		this.setPicturePath("style/img/activities/sport.png");
	}
	
}