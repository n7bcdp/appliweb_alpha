package activite.sortie;

import activite.Activite;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** Classe pour une activité de type Autre. */
@Entity
@DiscriminatorValue("AUTRE")
public class Autre extends Activite {

	public Autre() {
		this.setType("Autre");
		this.setPicturePath("style/img/activities/defaut.png");
	}
	
}