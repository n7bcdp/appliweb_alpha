package activite.sortie;

import activite.Activite;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** Classe pour une activité de type Culture. */
@Entity
@DiscriminatorValue("CULTURE")
public class Culture extends Activite {

	public Culture() {
		this.setType("Culture");
		this.setPicturePath("style/img/activities/culture.png");
	}
	
}
