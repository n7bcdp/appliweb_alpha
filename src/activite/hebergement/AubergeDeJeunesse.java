package activite.hebergement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Hebergement;

/** Classe pour un hébergement de type Auberge de Jeunesse. */
@Entity
@DiscriminatorValue("AUBERG.JEUN.")
public class AubergeDeJeunesse extends Hebergement {

	public AubergeDeJeunesse() {
		this.setType("Auberge de Jeunesse");
		this.setPicturePath("style/img/activities/youth_hostel.png");
	}
	
}