package activite.hebergement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Hebergement;

/** Classe pour un hébergement de type Chambre d'hôte. */
@Entity
@DiscriminatorValue("CHAM.HOTE")
public class ChambreHote extends Hebergement {

	public ChambreHote() {
		this.setType("Chambre d'hôte");
		this.setPicturePath("style/img/activities/guesthouse.png");
	}
	
}