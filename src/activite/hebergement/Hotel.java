package activite.hebergement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Hebergement;

/** Classe pour un hébergement de type Hôtel. */
@Entity
@DiscriminatorValue("HÔTEL")
public class Hotel extends Hebergement {

	public Hotel() {
		super();
		this.setType("Hotel");
		this.setPicturePath("style/img/activities/hotel.png");
	}
	
}