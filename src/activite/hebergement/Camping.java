package activite.hebergement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Hebergement;

/** Classe pour un hébergement de type Camping. */
@Entity
@DiscriminatorValue("CAMPING")
public class Camping extends Hebergement {

	public Camping() {
		this.setType("Camping");
		this.setPicturePath("style/img/activities/camping.png");
	}
	
}
