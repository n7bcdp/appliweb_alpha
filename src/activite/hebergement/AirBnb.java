package activite.hebergement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import activite.Hebergement;

/** Classe pour un hébergement de type AirBnB. */
@Entity
@DiscriminatorValue("AIRBNB")
public class AirBnb extends Hebergement {

	public AirBnb() {
		this.setType("AirBnB");
		this.setPicturePath("style/img/activities/airbnb.png");
	}
	
}