package voyage;
import activite.Activite;
import compte.Utilisateur;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Voyage {
	
	/** Identifiant unique du voyage. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	/** Titre du voyage. */
	private String titre;
	
	/** Description du voyage. */
	private String description;
	
	/** Date de début du voyage. */
	private String dateDebut;
	
	/** Date de fin du voyage. */
	private String dateFin;
	
	/** Vignette du voyage. */
	private String image;
	
	/** Activités du voyage. */
	@OneToMany(fetch=FetchType.EAGER, orphanRemoval=true)
	private List<Activite> activites;
	
	/** Participants du voyage. */
	@ManyToMany(fetch=FetchType.EAGER, mappedBy="voyages")	
	private List<Utilisateur> owners;

	public Voyage() {
		this.activites = new ArrayList<Activite>();
		
		this.owners = new ArrayList<Utilisateur>();
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/** Obtenir la date de début du voyage.
	 * @return le date de début
	 */
	public String getDateDebut() {
		return dateDebut;
	}

	/** Fixer la nouvelle date de début du voyage.
	 * @param _dateDebut la nouvelle date de début du voyage
	 */
	public void setDateDebut(String _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/** Obtenir la date de fin du voyage
	 * @return la date de fin du voyage
	 */
	public String getDateFin() {
		return dateFin;
	}

	/** Fixer la nouvelle date de fin du voyage
	 * @param _dateFin la nouvelle fin de début du voyage
	 */
	public void setDateFin(String _dateFin) {
		this.dateFin = _dateFin;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Collection<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	public Collection<Utilisateur> getOwners() {
		return owners;
	}

	public void setOwners(List<Utilisateur> owners) {
		this.owners = owners;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
}
