package voyage;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import activite.Activite;
import compte.Utilisateur;

@Singleton
public class FacadeGestionVoyage {

	@PersistenceContext
	public EntityManager em;

	/** Ajouter un voyage.
	 * @param v Un voyage
	 */
	public void addVoyage(Voyage v) {
		em.persist(v);
	}

	/** Ajouter une activité.
	 * @param a Une activité.
	 */
	public void addActivite(Activite a) {
		em.persist(a);
	}

	/** Associer un voyage déjà existant à un utilisateur.
	 * @param userId L'identifiant de l'utilisateur.
	 * @param voyageId L'identifiant du voyage.
	 */
	public void associerVoyage(int userId, int voyageID) {
		Voyage v = em.find(Voyage.class, voyageID);
		Utilisateur u = em.find(Utilisateur.class, userId);
		u.getVoyages().add(v);
		v.getOwners().add(u);
	}

	/** Supprimer un voyage d'un utilisateur. Si le voyage 
	 * n'a qu'un propriétaire, le supprime définitivement.
	 * @param userId Identifiant de l'utilisateur.
	 * @param voyageID Identifiant du voyage.
	 */
	public void supprimerVoyage(int userId, int voyageID) {
		Voyage v = em.find(Voyage.class, voyageID);
		Utilisateur u = em.find(Utilisateur.class, userId);
		if (u.getVoyages().contains(v)) {
			u.getVoyages().remove(v);
			if (v.getOwners().size() == 0) {
				em.remove(v);
			}
		}
	}

	/** Mettre a jour un voyage.
	 * @param v
	 */
	public void updateVoyage(Voyage v) {
		em.merge(v);
	}

	/** Ajouter une activité à un voyage.
	 * @param voyageID L'identifiant du voyage.
	 * @param activiteID L'identifiant de l'activité.
	 */
	public void ajouterActivite(int voyageID, int activiteID) {
		Voyage _v = em.find(Voyage.class, voyageID);
		Activite _a = em.find(Activite.class, activiteID);
		_v.getActivites().add(_a);
	}

	/** Supprimer une activité d'un voyage.
	 * @param activiteID Identifiant de l'activité.
	 */
	public void supprimerActivite(int voyageID, int activiteID) {
		Activite a = em.find(Activite.class, activiteID);
		Voyage v = em.find(Voyage.class, voyageID);
		v.getActivites().remove(a);
		em.remove(a);
	}
	
	/** Mettre a jour une activite.
	 * @param a
	 */
	public void updateActivite(Activite a) {
		em.merge(a);
	}

	/** Lister tous les voyages. */
	public Collection<Voyage> listerVoyage() {
		return em.createQuery("from Voyage", Voyage.class).getResultList();
	}
	
	/** Lister tous les voyages possédés par un utilisateur.
	 * @param ownerId Id de l'owner.
	 * @return La collection des voyages de l'utilisateur.
	 */
	public Collection<Voyage> listerVoyage(int ownerId) {
		Utilisateur u = em.find(Utilisateur.class, ownerId);	

		return u.getVoyages();
	}
	
	/** Récupérer un voyage via son id.	
	 * @param idVoyage L'id du voyage recherché.
	 * @return Le voyage ou null si il n'existe pas.
	 */
	public Voyage getVoyageById(int idVoyage) {
		return em.find(Voyage.class, idVoyage);
	}

	/** Lister toutes les activités. */
	public Collection<Activite> listerActivite() {
		return em.createQuery("from Activite", Activite.class).getResultList();
	}
	
	public Collection<Activite> listerActivite(int voyageId) {
		Voyage v = em.find(Voyage.class, voyageId);
		return v.getActivites();
	}
	/** Lister les participants d'un voyage */
	public Collection<Utilisateur> listerParticipants(int voyageId){
		Voyage v = em.find(Voyage.class, voyageId);
		return v.getOwners();
	}
	
	

}
