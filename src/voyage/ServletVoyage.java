package voyage;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import activite.Activite;
import activite.Hebergement;
import activite.Transport;
import activite.hebergement.*;
import activite.transport.*;
import activite.sortie.*;
import compte.CompteUtilisateur;
import compte.ServletCompte;
import outils.ExclusionStrategyImpl;

/** Servlet implementation class ServletVoyage. */
@WebServlet("/ServletVoyage")
public class ServletVoyage extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	FacadeGestionVoyage facade;

	public ServletVoyage() {
		super();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* Récupération de l'opération. */
		String op = request.getParameter("op");

		/* Récupération de la session. */
		HttpSession session = request.getSession();
		Object objetCompte = session.getAttribute(ServletCompte.ATT_SESSION_USER);		

		/* Toutes les opérations nécéssitent une connexion. */
		if (objetCompte == null) {
			/* Répondre un message d'erreur. */
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("{\"error\" : \"Acces Non Autorisé\"}");

		/* Connexion valide. */
		} else {
	
			CompteUtilisateur compte = (CompteUtilisateur) objetCompte;
			int id = compte.getIdCompte();

			/* Gson builder.*/
			Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategyImpl()).create();

			if (op == null || op == "") {
				response.sendRedirect("#/erreur/ErreurChampOP:Null");
			} else {
				switch(op) {
				/* ######### VOYAGES ######### */
				case "lister" :
					listerVoyage(response, id, gson);
					break;
				case "voir" :
					consulterVoyage(request, response, gson);
					break;
				case "supprimerVoyage" :
					supprimerVoyage(request, response, compte);
					break;
				case "nouveauVoyage" :
					request.getRequestDispatcher("creationvoyage.html").forward(request, response);					
					break;
				case "validerNouveauVoyage" :
					ajouterNouvVoyage(request, response, id);
					break;
				case "modifierVoyage" :
					modificationVoyage(request, response);
					break;
				/* ######### ACTIVITES ######### */
				case "nouvelleActivite" :
					creerActivite(request, response);
					break;
				case "editerActivite" :
					editerActivite(request, response);
					break;
				case "supprimerActivite" :
					supprimerActivite(request, response, compte);
					break;
				/* ######### PARTICIPANTS ######### */
				case "listerParticipants" :
					response.setContentType("text/html");
					try {
						int idV = Integer.parseInt(request.getParameter("idVoyage"));
						listerParticipantsVoyage(request, response, idV, gson);
						//response.getWriter().write(facade.listerParticipants(idV).size()+"");
						
					} catch(Exception e) {
						response.getWriter().write("error");
					}
					break;
				case "ajouterParticipant" :
					ajouterParticipant(request,response);
					break;

				default:
					response.sendRedirect("#/erreur/ErreurChampOP:NonReconnu");
					break;
				}
			}
		}
	}

	/** Ajouter un nouveau voyage depuis le formulaire de création.
	 * @param request la requête de la Servlet
	 * @param response la réponse de la Servlet
	 * @param id l'identifiant de l'utilisateur
	 * @throws IOException
	 */
	private void ajouterNouvVoyage(HttpServletRequest request, HttpServletResponse response, int id) throws IOException {
		String titre = request.getParameter("titre");
		String description = request.getParameter("description");
		if (titre.equals("")) {
			response.sendRedirect("#/erreur/ChampVide");
		} else {
			Voyage v = new Voyage();
			v.setTitre(titre);
			v.setDescription(description); // Si problème : ANCIENNEMENT DANS IF DESCRIPTION.EQUALS ""
			facade.addVoyage(v);
			facade.associerVoyage(id, v.getId());
			response.sendRedirect("#/voyage/" + v.getId());
		}
	}

	/** Consulter un voyage.
	 * @param request la requête de la Servlet dans laquelle on lit l'id. du voyage
	 * @param response la réponse de la Servlet dans laquelle on écrit le gson
	 * @param gson le gson builder pour donner le voyage à la page
	 * @throws IOException
	 */
	private void consulterVoyage(HttpServletRequest request, HttpServletResponse response, Gson gson) throws IOException {
		String idVoyage = request.getParameter("voyage");
		try {
			Voyage v = facade.getVoyageById(Integer.parseInt(idVoyage));
			if (v == null) { throw new Exception(); }
			String jsonVoyage = gson.toJson(v);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonVoyage);
		} catch(Exception e) {		
			response.sendRedirect("#/erreur/VoyageInexistant");				
		}
	}

	/** Lister les voyages d'un utilisateur.
	 * @param response la réponse de la Servlet dans laquelle on écrit le gson
	 * @param id l'identifiant de l'utilisateur
	 * @param gson le gson builder pour donner la liste de voyage
	 * @throws IOException
	 */
	private void listerVoyage(HttpServletResponse response, int id, Gson gson) throws IOException {
		String jsonListeVoyages = gson.toJson(facade.listerVoyage(id));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonListeVoyages);
	}

	/** Modifier un voyage.
	 * @param request la requête de la Servlet
	 * @param response la réponse de la Servlet
	 * @throws IOException
	 */
	private void modificationVoyage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String newTitre = request.getParameter("titre");
		String newDescription = request.getParameter("description");
		String idV = request.getParameter("voyage");
		try {
			if (!idV.equals("") && !newTitre.equals("")) {
				Voyage v = facade.getVoyageById(Integer.parseInt(idV));
				if (v != null) {
					//TODO : verifier que le voyage appartient bien à l'user connecté !!
					v.setTitre(newTitre);
					if (!newDescription.equals("")) {
						v.setDescription(newDescription);
					} else {
						v.setDescription("");
					}
					facade.updateVoyage(v);
					response.sendRedirect("#/voyage/" + idV);
				} else {
					throw new Exception();
				}
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			response.sendRedirect("#/erreur/ErreurModificationVoyage");
		}
	}

	/** Supprimer un voyage.
	 * @param request la requête HTTP du GET
	 * @param response la réponse HTTP du GET
	 */
	private void supprimerVoyage(HttpServletRequest request, HttpServletResponse response, CompteUtilisateur compte) {
		Integer idVoyage = Integer.parseInt(request.getParameter("idVoyage"));
		facade.supprimerVoyage(compte.getIdCompte(), idVoyage);		
	}

	/** Créer une activité dans un voyage.
	 * @param request la requête HTTP du GET
	 * @param response la réponse HTTP du GET
	 * @throws IOException
	 */
	private void creerActivite(HttpServletRequest request, HttpServletResponse response) throws IOException {

		Integer idVoyage = Integer.parseInt(request.getParameter("idvoyage"));

		/* On initialise tous les paramètres possibles. */
		HashMap<String,String> listeParametre = recupererParametre(request);
		
		/* On récupère le type de l'activité. */
		String categorie = request.getParameter("categorie");
		String subCategorie = request.getParameter("subCategorie");
		listeParametre.put("type", subCategorie);

		try {
			switch(categorie) {
			case "activite":
				creerSortie(response, listeParametre, idVoyage);
				break;

			case "transport":
				creerTransport(response, listeParametre, idVoyage);
				break;

			case "hebergement":
				creerHebergement(response, listeParametre, idVoyage);
				break;				
			}
		} catch (Exception e) {
			/* Rediriger vers une erreur. */
			String stack = "";
			for (int i = 0 ; i < e.getStackTrace().length ; i++) {
				stack += e.getStackTrace()[i].toString();
			}
			response.sendRedirect("#/erreur/Erreur:" + stack);
		}

	}

	/** Récupérer les paramètres de l'activité à créer.
	 * @param request la requête
	 * @return une HashMap<Nom du paramètre, paramètre>
	 */
	private HashMap<String, String> recupererParametre(HttpServletRequest request) {

		HashMap<String, String> listePar = new HashMap<String, String>();

		/* Paramètres communs à toutes les activités. */
		listePar.put("nom", request.getParameter("nom"));
		listePar.put("dateDebut", request.getParameter("dateDebut"));
		listePar.put("dateFin", request.getParameter("dateFin"));
		listePar.put("heureDebut", request.getParameter("heureDebut"));
		listePar.put("heureFin", request.getParameter("heureFin"));
		listePar.put("lieuDebut", request.getParameter("lieuDebut"));
		listePar.put("latitudeLieu", request.getParameter("latitudeLieu"));
		listePar.put("longitudeLieu", request.getParameter("longitudeLieu"));
		listePar.put("lieuFin", request.getParameter("lieuFin"));
		listePar.put("latitudeFin", request.getParameter("latitudeFin"));
		listePar.put("longitudeFin", request.getParameter("longitudeFin"));
		listePar.put("prix", request.getParameter("prix"));
		listePar.put("fichierPDF", request.getParameter("fichierPDF"));
		listePar.put("description", request.getParameter("description"));
		listePar.put("lienInternet", request.getParameter("lienInternet"));
		listePar.put("numTelephone", request.getParameter("numTelephone"));
		listePar.put("picturePath", request.getParameter("picturePath"));

		/* Paramètres communs à Héberg. et Transport. */
		listePar.put("numReservation", request.getParameter("numReservation"));

		/* Paramètre spécifique à Héberg. */
		listePar.put("adresse", request.getParameter("adresse"));

		/* Paramètre spécifique à Transport. */
		listePar.put("numPlace", request.getParameter("numPlace"));
		listePar.put("numTransport", request.getParameter("numTransport"));
		listePar.put("compagnieTransport", request.getParameter("compagnieTransport"));
		
		/* Autres paramètres. */
		listePar.put("nomHotel", request.getParameter("nomHotel"));

		return listePar;
	}

	/** Créer une activité de type Sortie ou Autre.
	 * @param response la réponse de la Servlet
	 * @param listeParametre la HashMap contenant les paramètres
	 * @param idVoyage l'identifiant du voyage
	 * @throws IOException
	 */
	private void creerSortie(HttpServletResponse response, HashMap<String, String> listeParametre, Integer idVoyage) throws IOException {
		
		String lieuDebut = listeParametre.get("lieuDebut");

		Activite a = null;
		
		switch(listeParametre.get("type")) {
		case "culture":
			a = new Culture();
			a.setNom("Visite : " + lieuDebut);
			break;
		case "sport":
			a = new Sport();
			a.setNom("Sport à " + lieuDebut);
			break;
		case "autre-sortie" :
			a = new Autre();
			a.setNom("Activité à " + lieuDebut);
			break;
		default:
			a = new Activite();
			break;
		}

		String latitudeLieu = listeParametre.get("latitudeLieu");
		String longitudeLieu = listeParametre.get("longitudeLieu");
		
		a.setDescription(listeParametre.get("description"));
		a.setDateDebut(listeParametre.get("dateDebut"));
		a.setDateFin(listeParametre.get("dateFin"));
		a.setHeureDebut(listeParametre.get("heureDebut"));
		a.setHeureFin(listeParametre.get("heureFin"));
		
		a.setPrix(Integer.parseInt(listeParametre.get("prix")));
		a.setLienInternet(listeParametre.get("lienInternet"));
		a.setNumeroTelephone(listeParametre.get("numTelephone"));
		
		a.setNomLieuDebut(lieuDebut);
		a.setNomLieuFin(lieuDebut);
		a.setLatitudeLieu(latitudeLieu);
		a.setLatitudeFin(latitudeLieu);
		a.setLongitudeLieu(longitudeLieu);
		a.setLongitudeFin(longitudeLieu);
		
		facade.addActivite(a);
		facade.ajouterActivite(idVoyage, a.getId());
		response.sendRedirect("#/voyage/" + idVoyage);	
	}
	
	/** Créer une activité de type Transport.
	 * @param response la réponse de la Servlet
	 * @param listeParametre la HashMap contenant les paramètres
	 * @param idVoyage l'identifiant du voyage
	 * @throws Exception 
	 */
	private void creerTransport(HttpServletResponse response, HashMap<String, String> listeParametre, Integer idVoyage) throws Exception {

		String numPlace = listeParametre.get("numPlace");
		String numTransport = listeParametre.get("numTransport");
		String compagnieTransport = listeParametre.get("compagnieTransport");

		Transport t = null;

		switch(listeParametre.get("type")) {
		case "ferry":
			t = new Bateau();
			t.setNom("Transport en bateau");
			break;
		case "bus":
			t = new Bus();
			t.setNom("Transport en bus");
			t.setCompagnieTransport(compagnieTransport);
			t.setNumeroPlace(numPlace);
			t.setNumeroTransport(numTransport);
			break;
		case "covoiturage":
			t = new Covoiturage();
			t.setNom("Covoiturage");
			break;
		case "metro":
			t = new Metro();
			t.setNom("Transport en bus");
			break;
		case "pieds":
			t = new Pied();
			t.setNom("Marche");
			break;
		case "taxi":
			t = new Taxi();
			t.setNom("Taxi");
			break;
		case "train":
			t = new Train();
			t.setNom("Train");
			t.setCompagnieTransport(compagnieTransport);
			t.setNumeroPlace(numPlace);
			t.setNumeroTransport(numTransport);
			break;
		case "voiture":
			t = new Voiture();
			t.setNom("Voiture");
			break;
		case "vol":
			t = new Vol();
			t.setNom("Vol");
			t.setCompagnieTransport(compagnieTransport);
			t.setNumeroPlace(numPlace);
			t.setNumeroTransport(numTransport);
			break;
		default:
			t = new Transport();
			t.setNom("Transport");
			break;
		}

		String latitudeLieu = listeParametre.get("latitudeLieu");
		String longitudeLieu = listeParametre.get("longitudeLieu");
		
		String latitudeFin = listeParametre.get("latitudeFin");
		String longitudeFin = listeParametre.get("longitudeFin");

		t.setDateDebut(listeParametre.get("dateDebut"));
		t.setDateFin(listeParametre.get("dateFin"));

		t.setHeureDebut(listeParametre.get("heureDebut"));
		t.setHeureFin(listeParametre.get("heureFin"));
		
		t.setNomLieuDebut(listeParametre.get("lieuDebut"));
		t.setNomLieuFin(listeParametre.get("lieuFin"));
		t.setNom(t.getNom().concat(" de " + t.getNomLieuDebut() + " à " + t.getNomLieuFin()));
		
		t.setLatitudeLieu(latitudeLieu);
		t.setLatitudeFin(latitudeFin);
		t.setLongitudeLieu(longitudeLieu);
		t.setLongitudeFin(longitudeFin);
		
		t.setPrix(Integer.parseInt(listeParametre.get("prix")));
		t.setLienInternet(listeParametre.get("lienInternet"));
		t.setNumeroTelephone(listeParametre.get("numTelephone"));

		facade.addActivite(t);
		facade.ajouterActivite(idVoyage, t.getId());

		response.sendRedirect("#/voyage/" + idVoyage);
	}
	
	/** Créer une activité de type Hébergement.
	 * @param response la réponse de la Servlet
	 * @param listeParametre la HashMap contenant les paramètres
	 * @param idVoyage l'identifiant du voyage
	 * @throws Exception 
	 */
	private void creerHebergement(HttpServletResponse response, HashMap<String, String> listeParametre, Integer idVoyage) throws Exception {

		String lieuDebut = listeParametre.get("lieuDebut");
		
		Hebergement h = null;

		switch(listeParametre.get("type")) {
		case "airbnb":
			h = new AirBnb();
			h.setNom("Réservation AirBnB à" + lieuDebut);
			break;
		case "auberge-jeunesse":
			h = new AubergeDeJeunesse();
			h.setNom("Réservation à l'auberge " + lieuDebut);
			break;
		case "camping":
			h = new Camping();
			h.setNom("Réservation au camping " + lieuDebut);
			break;
		case "chambre-hote":
			h = new ChambreHote();
			h.setNom("Réservation chez " + lieuDebut);
			break;
		case "hotel":
			h = new Hotel();
			h.setNom("Réservation au " + lieuDebut);
			break;
		default:
			h = new Hebergement();
			h.setNom("Réservation de l'hébergement " + lieuDebut);
			break;
		}
		
		String dateDebut = listeParametre.get("dateDebut");
		String dateFin = listeParametre.get("dateFin");
		String latitudeLieu = listeParametre.get("latitudeLieu");
		String longitudeLieu = listeParametre.get("longitudeLieu");
		
		/* Afficher le nombre de nuits dans la description. */
		DateFormat sourceFormat = new SimpleDateFormat("YYYY-MM-dd");
		Date date1 = sourceFormat.parse(dateDebut);
		Date date2 = sourceFormat.parse(dateFin); //TODO
		long diff = date2.getTime() - date1.getTime();
		h.setDescription((TimeUnit.DAYS.convert(diff, TimeUnit.DAYS)-1) + " nuits");

		h.setDateDebut(dateDebut);
		h.setDateFin(dateFin);
		h.setHeureDebut(listeParametre.get("heureDebut"));
		h.setHeureFin(listeParametre.get("heureFin"));

		h.setNomLieuDebut(lieuDebut);
		h.setNomLieuFin(lieuDebut);
		h.setAdresse(listeParametre.get("adresse"));

		h.setLatitudeLieu(latitudeLieu);
		h.setLatitudeFin(latitudeLieu);
		h.setLongitudeLieu(longitudeLieu);
		h.setLongitudeFin(longitudeLieu);

		h.setLienInternet(listeParametre.get("lienInternet"));
		h.setPrix(Integer.parseInt(listeParametre.get("prix")));
		h.setNumeroReservation(Integer.parseInt(listeParametre.get("numReservation")));
		h.setNumeroTelephone(listeParametre.get("numTelephone"));

		facade.addActivite(h);
		facade.ajouterActivite(idVoyage, h.getId());
		response.sendRedirect("#/voyage/" + idVoyage);
	}

	/** Mettre à jour les dates bornes du voyage.
	 * @param idVoyage l'identifiant du voyage
	 * @param activite l'activité qui peut faire faire la maj
	 */
	private void majDateVoyage(Integer idVoyage, Activite activite) {
		Voyage _v = facade.getVoyageById(idVoyage);
		SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd");
		try {
			/* On récupère les dates dans le bon format. */
			Date voyageDeb = format.parse(_v.getDateDebut());
			Date voyageFin = format.parse(_v.getDateFin());
			Date activiteDeb = format.parse(activite.getDateDebut());
			Date activiteFin = format.parse(activite.getDateFin());
			/* On cherche à faire une mise à jour. */
			if (activiteDeb.compareTo(voyageDeb) <= 0) {
				_v.setDateDebut(activite.getDateDebut());
			}
			if (voyageFin.compareTo(activiteFin) <= 0) {
				_v.setDateDebut(activite.getDateDebut());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		facade.updateVoyage(_v);
	}
	
	/** Lister les participants d'un voyage */
	public void listerParticipantsVoyage(HttpServletRequest request, HttpServletResponse response, int id, Gson gson) {
		try {
			String jsonListeParticipants = gson.toJson(facade.listerParticipants(id));
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonListeParticipants);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}

	/** Editer une activité d'un voyage.
	 * @param request la requête HTTP du GET
	 * @param response la réponse HTTP du GET
	 */
	private void editerActivite(HttpServletRequest request, HttpServletResponse response) {
		HashMap<String, String> listeParametre = recupererParametre(request);
		/* On récupère les infos nécessaires à l'édition. */
		Integer idVoyage = Integer.parseInt(request.getParameter("idVoyage"));
		Integer idAct = Integer.parseInt(request.getParameter("idActivite"));
		Activite activite = null;
		for (Activite a : facade.getVoyageById(idVoyage).getActivites()) {
			if (a.getId() == idAct) {
				activite = a;
			}
		}
		activite.setNom(listeParametre.get("nom"));
		facade.updateActivite(activite);
		try {
			response.sendRedirect("#/voyage/" + idVoyage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Supprimer une activité d'un voyage.
	 * @param request la requête HTTP du GET
	 * @param response la réponse HTTP du GET
	 */
	private void supprimerActivite(HttpServletRequest request, HttpServletResponse response, CompteUtilisateur compte) {
		/* On récupère les infos nécessaires à la suppression. */
		Integer idAct = Integer.parseInt(request.getParameter("idActivite"));
		Integer idVoyage = Integer.parseInt(request.getParameter("idVoyage"));
		/* On supprime si le voyage propriétaire de l'activité appartient à l'utilisateur. */
		// TODO Vérifier si le voyage appartient bien au compte connecté !
		facade.supprimerActivite(idVoyage, idAct);
		/* On redirige. */
		try {
			response.sendRedirect("#/voyage/" + idVoyage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/** Ajout d'un participant à un voyage 
	 * @throws IOException */
	
	private void ajouterParticipant(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int idVoyage = Integer.parseInt(request.getParameter("idVoyage"));
		int idParticipant = Integer.parseInt(request.getParameter("idNouveauParticipant"));
		facade.associerVoyage(idParticipant, idVoyage);
		try {
			response.sendRedirect("#/participants/"+idVoyage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			String stack = "";
			for (int i = 0 ; i < e.getStackTrace().length ; i++) {
				stack += e.getStackTrace()[i].toString();
			}
				response.sendRedirect("#/erreur/Erreur:" + stack);
		}
		
	}
}
