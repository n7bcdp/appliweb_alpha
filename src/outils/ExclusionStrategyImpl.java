package outils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import compte.Utilisateur;
import voyage.Voyage;

public class ExclusionStrategyImpl implements ExclusionStrategy {

	@Override
	public boolean shouldSkipClass(Class<?> c) {
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return ((f.getDeclaringClass() == Voyage.class && f.getName().equals("owners"))
				|| (f.getDeclaringClass() == Utilisateur.class && f.getName().equals("amis")) ||
				(f.getDeclaringClass() == Utilisateur.class && f.getName().equals("voyages")));
	}

}
