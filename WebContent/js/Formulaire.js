/*Manipulation du DOM pour les formulaires de connexion/inscription/modification */

/*Fonction annexe */
function surligne(champ, erreur)
{
   if(erreur)
      champ.style.backgroundColor = "#fba";
   else
      champ.style.backgroundColor = "";
}



function verifMDP(champ) {
	if (champ.value.length >3 && champ.value.length < 13){
		surligne(champ,false);
		return true;
	}
	else {
		surligne(champ,true);
		return false;
	}
	
	
}


function verifNom(champ) {
	if (champ.value.length >2){
		surligne(champ,false);
		return true;
	}
	else {
		surligne(champ,true);
		return false;
	}
	
	
}


function verifMail(champ)
{
   var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
   if(!regex.test(champ.value))
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}


function verifForm(f)
{
   var NomOk = verifNom(f.registerFirstName);
   var MailOk = verifMail(f.registerEMail);
   var MDPOk = verifMDP(f.registerPassword);
   
   if(MDPOk && NomOk && MailOk) {
	   return true;
   }
   else
   {
	  balise = document.createTextNode("Formulaire incorrect");
	  document.getElementById("monTest").appendChild(balise);
      return false;
   }
}
