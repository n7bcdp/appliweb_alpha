var app = angular.module('myApp2',['ngRoute', 'routeAppControllers']);
var routeAppControllers = angular.module('routeAppControllers', []);


/*Routeur */

app.config(['$routeProvider', '$locationProvider',
	function($routeProvider, $locationProvider) {
	// Système de routage
	$routeProvider
	.when('/profil/:id', {
		templateUrl: 'profil.html',
		controller: 'afficherProfilController'
	})
	.when('/utilisateurs', {
		templateUrl : 'Test.html',
		controller: 'utilisateursController'
	})
	.otherwise({
		redirectTo: '/utilisateurs'
	});
	$locationProvider.hashPrefix('');
}	
]);





app.controller('utilisateursController',function($scope,$http,$window){
	
	$http.get("/appliWebAlpha/ServletCompte?op=listerUtilisateurs").then(function(response) {

		var PagesUtilisateur = response.data;
		$scope.listePages = [[PagesUtilisateur[0],PagesUtilisateur[1]],[PagesUtilisateur[2]]]
		$scope.utilisateursPage = $scope.listePages[0];
				
	});
	
	$http.get("/appliWebAlpha/ServletCompte?op=RecupererInfos").then(function(response) {
		
		$scope.myID = response.data.idUtilisateur;
	});
		
	$scope.nouvellePage = function(p) {
		$scope.utilisateursPage = p;
	}
	
	$scope.redirection = function() {
		$window.location.href = 'connexion.jsp';
	};
	
	
	
});


app.controller('afficherProfilController',function($scope,$routeParams,$http){

	$scope.userId = $routeParams.id;
	
	$http.get("/appliWebAlpha/ServletCompte?op=RecupererInfosEtranger&idUser=" + $scope.userId).then(function(response) {
		$scope.user = response.data;
	});
	
	$http.get("/appliWebAlpha/ServletCompte?op=listerAmis").then(function(response) {
		var listeAmis = response.data;
		$scope.Bouton=true;
		for (i = 0; i < listeAmis.length; i++) {

			if (listeAmis[i].idUtilisateur == $scope.userId){
				$scope.Bouton=false;
			}
			
		}
		
	});
	
	
	
	
	
	
});
	
app.controller('connexionController', function($scope, $http) {
	$scope.connecte = false;
	$http.get("/appliWebAlpha/ServletCompte?op=estConnecte").then(function(response) {
		$scope.connexion = response.data;
		if ($scope.connexion.statut === "connecte") {
			$scope.connecte = true;
		}
	});
		
		
});
		
		
		

	
	
