/* Pour le Carousel. */
var n = 3;
var app = angular.module('myApp', ['ngRoute', 'routeAppControllers', 'ngMessages']);
var routeAppControllers = angular.module('routeAppControllers', []);

/**
 * Configuration du module principal : routeApp
 */
app.config(['$routeProvider', '$locationProvider', '$sceDelegateProvider',
	function($routeProvider, $locationProvider, $sceDelegateProvider) {
	$sceDelegateProvider.resourceUrlWhitelist([
  	    // Allow same origin resource loads.
  	    'self',
  	    // Allow loading from our assets domain.  Notice the difference between * and **.
  	    'https://flightxml.flightaware.com/json/FlightXML3/**'
  	  ]);
	
	// Système de routage
	$routeProvider
	.when('/home', {
		templateUrl: 'includes/carousel.html',
		controller: 'carouselController'
	})
	.when('/voyages', {
		templateUrl: 'mesvoyages.html',
		controller: 'listerVoyagesController'
	})
	.when('/voyage/:id', {
		templateUrl: 'voyage.html',
		controller: 'afficherVoyageController'
	})
	.when('/nouveau', {
		templateUrl: 'creationvoyage.html',
		controller: 'creationVoyageController'
	})
	.when('/participants/:id', {
		templateUrl: 'listeParticipants.html',
		controller: 'listeParticipantsController'
	})
	.when('/erreur/:msg', {
		templateUrl: 'erreur.html',
		controller: 'erreurController'
	})
	.when('/compte', {
		templateUrl: 'ServletCompte?op=versModification',
	})
	.when('/friends',{
		templateUrl: 'friends.html',
		controller : 'friendsController',
	})
	.when('/utilisateurs', {
		templateUrl : 'Test.html',
		controller : 'utilisateursController'
	})
	.otherwise({
		redirectTo: '/home'
	});
	$locationProvider.hashPrefix('');
}	
]);

/** Contrôleur pour le Carousel de la page d'accueil. */
app.controller('carouselController',function($scope, $interval) {
	var autoNext = function () {
		$scope.next();
	}
	$interval(autoNext, 4000); 

	$scope.images = [
		{
			image : '0.jpg',
			caption : 'Préparez votre voyage dans les moindres détails.'
		},
		{
			image : '1.jpg',
			caption : 'Toutes les informations dont vous avez besoin regroupées au même endroit.'
		},
		{
			image : '2.jpg',
			caption : 'Partagez vos expériences avec vos amis.'
		} ];
	$scope.currentImage = $scope.images[0];


	$scope.previous = function() {
		var newId = $scope.getCurrentImageId();
		if (newId > 0) {
			newId--;
		}
		$scope.currentImage = $scope.images[newId];
	};

	$scope.next = function() {
		var newId = $scope.getCurrentImageId();
		if (newId < n - 1) {
			newId++;
		} else {
			newId = 0;
		}
		$scope.currentImage = $scope.images[newId];
	};

	$scope.set = function(img) {
		$scope.currentImage = img;
	};

	$scope.getCurrentImageId = function() {
		return $scope.images
		.findIndex($scope.isCurrentImage);
	};

	$scope.isCurrentImage = function(img) {
		return img == $scope.currentImage;
	};
});

/** Lister les voyages d'un utilisateur. */
app.controller('listerVoyagesController', function($window, $scope, $http) {
	$scope.ready = false;
	$http.get("/appliWebAlpha/ServletVoyage?op=lister").then(function(response) {
		$scope.voyages = response.data;
		$scope.ready = true;

	});

	$scope.redirect = function() {
		$window.location.href = 'connexion.jsp';
	};

});

/** Afficher un voyage. */
app.controller('afficherVoyageController', function($window, $routeParams, $scope, $http, $timeout, $jsonpCallbacks) {
	var service = new google.maps.places.PlacesService(document.createElement('div'));
	var map;
	/* Pour charger la carte après 1,5 sec */
	$timeout(initMap, 1500);
	
	/* Récupération du voyage. */
	$scope.voyageId = $routeParams.id;
	$scope.ready = false;
	$http.get("/appliWebAlpha/ServletVoyage?op=voir&voyage="+$scope.voyageId).then(function(response) {
		$scope.voyage = response.data;
		$scope.ready = true;
	});	
	

	$scope.flightFound = false;
	/* Recherche d'un vol. */
	$scope.getFlightInfo = function(flightNum) {
		var auth = 'bHdhY2s6ZDc0MDU2NTVmMTA1MzkxZTRjZWZiNGRkMzk5ZDMzZjYwYzExYWFjZQ==';	  	
	  	var url = 'https://flightxml.flightaware.com/json/FlightXML3/FlightInfoStatus?ident=' + flightNum + '&howMany=0';
	  	$http({
	  	    method: 'JSONP',
	  	    url: url,
	  	    jsonpCallbackParam: 'jsonp_callback',
	  	    headers: {
	  	        'Authorization': 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==',
	  	        'Accept': 'application/json'
	  	    }
	  	}).then(function(response) {});
	}
	
	JsonpCallback = function(data){
		$scope.flightFound = true;
		$scope.flight = data.FlightInfoStatusResult.flights[0];
	};
	
	
	/* Google Maps. Affichage d'une carte. */
	function initMap() {
		if ($scope.voyage.activites.length > 0) {		
		
			/* Création de la carte centrée sur la première activité. */
			var lat = Number($scope.voyage.activites[0].latitudeLieu);
    		var lng = Number($scope.voyage.activites[0].longitudeLieu);
    		var pos = {lat: lat, lng: lng};
	        var map = new google.maps.Map(document.getElementById('map'), {
	          zoom: 3,
	          center: pos
	        });
	        
	        /* Ajout des marqueurs. */
	        for (var i = 0; i < $scope.voyage.activites.length; i++) {      	
	        	lat = Number($scope.voyage.activites[i].latitudeLieu);
	    		lng = Number($scope.voyage.activites[i].longitudeLieu);
	    		pos = {lat: lat, lng: lng};
	    		
	    		/* Cas d'un Hotel. */
	    		if ($scope.voyage.activites[i].type == 'Hotel' || $scope.voyage.activites[i].type == 'Chambre-hote') {
		    		marker = new google.maps.Marker( { 	position: pos,
		    									map: map, 
		    									icon: 'style/img/map-icons/sleep.png'
		    		});
		    		
		    		setInfoBulle(marker, $scope.voyage.activites[i].adresse);
		    		
		    	/* Cas d'un Vol. */
	    		} else if ($scope.voyage.activites[i].type == 'Vol') {
	    			pos2 = {lat: Number($scope.voyage.activites[i].latitudeFin), lng: Number($scope.voyage.activites[i].longitudeFin)};

	    			/* Ajout du parcours aerien. */
	    			var flightPlanCoordinates = [
	    		          pos,
	    		          pos2
	    		        ];
	    			var flightPath = new google.maps.Polyline({
	    		          path: flightPlanCoordinates,
	    		          geodesic: true,
	    		          strokeColor: '#1b5dc6',
	    		          strokeOpacity: 1.0,
	    		          strokeWeight: 3
	    		    });
	    		    flightPath.setMap(map);
	    		    setInfoBulle(flightPath, $scope.voyage.activites[i].nom);

	    		} else if ($scope.voyage.activites[i].type == 'Culture'){
	    			marker = new google.maps.Marker({
	    				position: pos,
						map: map, 
						icon: 'style/img/map-icons/museum.png'
	    			});
	    			setInfoBulle(marker, $scope.voyage.activites[i].adresse);
	    		}
	        }
        
		}
	};
    
    function setInfoBulle(marker, message) {
        var infowindow = new google.maps.InfoWindow({
          content: message
        });

        marker.addListener('click', function() {
          infowindow.open(marker.get('map'), marker);
        });
    }


	$scope.redirect = function() {
		$window.location.href = 'connexion.jsp';
	};


	// Pour afficher le formulaire.
	$scope.formulaire = false;
	$scope.afficherFormulaire = function(){
		$scope.formulaire = !$scope.formulaire;
	};

	/* Choix des catégories d'activité. */
	$scope.changerCategorie = function(categorie) {
		$scope.categorie = categorie;		
	};

	$scope.changerSubCategorie = function(subCategorie) {
		$scope.subCategorie = subCategorie;
	};

	/* Suppression d'une activité. */
	$scope.activiteSupprime = ""; // activité à supprimer

	$scope.showFormulaireSuppression = function(a) {
		if ($scope.activiteSupprime.id == a.id) {
			return true;
		} else {
			return false;
		}
	};

	$scope.annulerSuppressionActivite = function() {
		$scope.activiteSupprime = "";
	};

	$scope.supprimerActivite = function(a) {
		$scope.activiteSupprime = a;
	};

	$scope.actionSupprimerActivite = function() {
		$http.get("/appliWebAlpha/ServletVoyage?op=supprimerActivite" +
				"&idActivite=" + $scope.activiteSupprime.id + 
				"&idVoyage=" + $scope.voyageId).then(function(response) {
					$window.location.reload();
				});

	};

	/* Edition d'une activite. */
	$scope.activiteEdite = ""; // activité en cours d'édition.

	$scope.showFormulaireEdition = function(a) {
		if ($scope.activiteEdite.id == a.id) {
			return true;
		} else {
			return false;
		}
	};

	$scope.editerActivite = function(a) {
		$scope.activiteEdite = a;
	};

	$scope.annulerEditionActivite = function() {
		$scope.activiteEdite = "";
	};
	
	/* Pour les recherches de lieux. */
	$scope.googleResult = "";
	$scope.activiteAdresse = "";
	$scope.activiteTelephone = "";
	$scope.activiteInternet = "";
	$scope.activiteLieuFin = "";
	$scope.activiteLatitude = "";
	$scope.activiteLongitude = "";
	$scope.activiteLatitudeFin = "";
	$scope.activiteLongitudeFin = "";
	/* Compléter les informations du formulaire. */
	$scope.setInfo = function(info) {
		$scope.activiteAdresse = info.formatted_address;
		$scope.lieu = info.name + ", " + info.formatted_address;
		$scope.activiteLieuDebut = info.name;
		$scope.showResults = false;
		/* Ajouter des détails supplémentaires. */
		service.getDetails({placeId: info.place_id}, callbackDetail);
	};

	$scope.count = 0;
	$scope.showResults = true;
	
	$scope.findLieu = function(motcle) {
		$scope.count++;
		$scope.showResults = true;
		if ($scope.subCategorie === 'hotel') {
			$scope.findHotel(motcle)
		} else {
			var request = {query: motcle};
			service.textSearch(request, callbackSearch);
		}
	}
	
	/* Recherche aéroports. */
	$scope.findAirport = function(outbound, inbound) {
		$scope.numeroTransport = $scope.flight.ident;
		$scope.compagnieTransport = $scope.flight.airline;
		$scope.heureDebut = $scope.flight.estimated_departure_time.time;
		$scope.heureFin = $scope.flight.estimated_arrival_time.time;
		
		var request1 = {
				query: outbound,
				type : 'airport'
			};	
		service.textSearch(request1, callbackAirportOut);
		var request2 = {
				query: inbound,
				type : 'airport'
			};	
		service.textSearch(request2, callbackAirportIn);
	}
	
	function callbackAirportOut(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			
			$scope.activiteAdresse = results[0].formatted_address;
			$scope.activiteTelephone = "";
			$scope.activiteInternet = "";
			
			$scope.lieu = results[0].name;
			$scope.activiteLieuDebut = results[0].name;
			$scope.activiteLatitude = results[0].geometry.location.lat();
			$scope.activiteLongitude = results[0].geometry.location.lng();
		}
	};
	
	function callbackAirportIn(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			$scope.activiteLieuFin = results[0].name;			
			$scope.activiteLatitudeFin = results[0].geometry.location.lat();
			$scope.activiteLongitudeFin = results[0].geometry.location.lng();
		}
	};
	/* ******************* */

	$scope.findHotel = function(motcle) {
		var request = {
			query: motcle,
			type : 'lodging'
		};	
		service.textSearch(request, callbackSearch);
	};

	function callbackSearch(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			$scope.googleResults = results;
		} else {
			$scope.googleResults = null;
			$scope.showResults = false;
		}
	};	

	function callbackDetail(place, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			$scope.activiteLatitude = place.geometry.location.lat();
			$scope.activiteLongitude = place.geometry.location.lng();
			$scope.activiteTelephone = place.international_phone_number;
			$scope.activiteInternet = place.website;
			$scope.activiteLieuFin = place.name;
			$scope.activiteLatitudeFin = place.geometry.location.lat();
			$scope.activiteLongitudeFin = place.geometry.location.lng();
		}
	};

	/* Edition voyage (pop-up) */
	$scope.showPopUpEditionVoyage = function() {
		$scope.stylePopUp = "display:block;";
		$scope.editionAction = "ServletVoyage?op=modifierVoyage&voyage=" + $scope.voyageId;
		$scope.editionTitre = $scope.voyage.titre;
		$scope.editionDescription = $scope.voyage.description;
	};

	$scope.fermerPopUpEditionVoyage = function() {
		$scope.stylePopUp = "";
	};
	$scope.titreColor = "#16bf22";
	$scope.checkTitre = function(titre) {
		if (titre.length > 3) {
			$scope.titreColor = "#16bf22";
		} else {
			$scope.titreColor = "red";
		}
	};


	/* Suppression voyage. */
	$scope.showSupprimerVoyage = false;
	$scope.supprimerVoyage = function() {
		$scope.showSupprimerVoyage = true;
	};
	$scope.actionSupprimerVoyage = function() {
		$http.get("/appliWebAlpha/ServletVoyage?op=supprimerVoyage" +
				"&idVoyage=" + $scope.voyageId).then(function(response) {
					$window.location.href = '#/voyages';
				});
	};
	$scope.annulerSuppressionVoyage = function() {
		$scope.showSupprimerVoyage = false;
	};
});

/** Liste participants d'un voyage */
app.controller('listeParticipantsController', function($scope, $routeParams, $http, $window) {
	
	/* Affichage des participants */
	$scope.ready = false;
	$scope.idVoyage = $routeParams.id;
	$http.get("/appliWebAlpha/ServletVoyage?op=listerParticipants&idVoyage="+$scope.idVoyage).then(function(response) {
		$scope.participants = response.data;
		$scope.ready = true;
	});
	
	$http.get("/appliWebAlpha/ServletCompte?op=listerAmis").then(function(response) {
		$scope.amis = response.data;
		$scope.ready = true;
	});
	
	$http.get("/appliWebAlpha/ServletCompte?op=listerUtilisateurs").then(function(response){
		$scope.utilisateurs = response.data;
		$scope.ready = true;
	});
	
	
	$scope.redirect = function() {
		$window.location.href = 'connexion.jsp';
	};
});


/** Contrôleur pour les erreurs. */
app.controller('erreurController', function($routeParams, $scope) {
	$scope.erreur = $routeParams.msg;
});

/** Contrôleur pour la création d'un nouveau voyage. */
app.controller('creationVoyageController', function($scope) {

	$scope.titreColor = "red";
	$scope.checkTitre = function(titre) {
		if (titre.length > 3) {
			$scope.titreColor = "#16bf22";
		} else {
			$scope.titreColor = "red";
		}
	}
});

app.controller('connexionController', function($scope, $http) {
	$scope.connecte = false;
	$http.get("/appliWebAlpha/ServletCompte?op=estConnecte").then(function(response) {
		$scope.connexion = response.data;
		if ($scope.connexion.statut === "connecte") {
			$scope.connecte = true;
		}
	});
});


app.controller('formulaireActiviteController', function($scope){
	$scope.nbOnglets = document.getElementsByClassName("tab");	// Nombre d'onglets
	$scope.ongletCourant = 0;									// L'onglet courant

	showTab($scope.ongletCourant);	// Afficher l'onglet courant

	/** Afficher le bon onglet.
	 * @param n le numéro de l'onglet
	 */
	function showTab(n) {
		$scope.nbOnglets[n].style.display = "block";
		// Adapter les boutons
		if (n == 0) {
			document.getElementById("prev").style.display = "none";
		} else {
			document.getElementById("prev").style.display = "inline";
		}
		if (n == ($scope.nbOnglets.length - 1)) {
			document.getElementById("next").style.display = "none";
		} else {
			document.getElementById("next").style.display = "inline";
		}
	}

	/** Avancer. */
	$scope.avancer = function (n) {
		if (n == 1 && !validateForm()) { 
			return false;
		}
		$scope.nbOnglets[$scope.ongletCourant].style.display = "none";
		$scope.ongletCourant = $scope.ongletCourant + n;
		if (n === '-1') {
			document.getElementsByClassName("step")[$scope.ongletCourant].className = "step";
		}
		showTab($scope.ongletCourant);
	}

	/** Valider l'onglet. */ 
	function validateForm() {
		var valid = true;
		var i, y = 0;
		// Vérifier tous les "select"
		var y = $scope.nbOnglets[$scope.ongletCourant].getElementsByTagName("select");
		for (i = 0; i < y.length; i++) {
			if (y[i].value == "") {
				y[i].className += " invalid";	// Pour la coloration du champ
			    valid = false;					// Pour le statut
			}
		}
		// Vérifier tous les "input"
		y = $scope.nbOnglets[$scope.ongletCourant].getElementsByTagName("input");
		for (i = 0; i < y.length; i++) {
			// Si le champ est vide
		    if (y[i].value === "") {
		    	// Vérifier si c'est normal ou non
		    	if (!(y[i].name === 'lienInternet' || y[i].name === 'numTelephone')) {
			    	if ($scope.categorie === 'transport') {
			    		if (!(y[i].name === 'nom') || (!$scope.subCategorie === 'vol' && y[i].name === 'rechercheVol')) {
			    			y[i].className += " invalid";	// Pour la coloration du champ
				    		valid = false;					// Pour le statut
			    		}
			    	} else if ($scope.categorie === 'hebergement') {
		    			if (!(y[i].name === 'nom' || y[i].name === 'adresse' || y[i].name === 'numeroTransport'
		    				|| y[i].name === 'numeroPlace' || y[i].name === 'compagnieTransport' || y[i].name === 'rechercheVol')) {
		    				y[i].className += " invalid";
				    		valid = false;
			    		}
			    	} else {
			    		if ($scope.categorie === 'activite') {
				    		if (!(y[i].name === 'numReservation' || y[i].name === 'numeroTransport'
				    			|| y[i].name === 'numeroPlace' || y[i].name === 'compagnieTransport' || y[i].name === 'rechercheVol')) {
					    		y[i].className += " invalid";
					    		valid = false;
				    		}
			    		}
			    	}
		    	}
		    }
		}
		if (valid) {
			document.getElementsByClassName("step")[$scope.ongletCourant].className += " finish";
		}
		return valid;
	}

});

app.controller('friendsController', function($scope, $http) {
	$scope.ready = false;
	$http.get("/appliWebAlpha/ServletCompte?op=listerAmis").then(function(response) {
		$scope.amis = response.data;
		$scope.ready = true;
	});
	
});


app.controller('utilisateursController', function($scope, $http) {
});

app.controller('editionActiviteController', function($scope) {
	$scope.activite = $scope.a;
	$scope.edition = false;
	
	$scope.editable = function() {
		$scope.edition = true;
	}
	
	$scope.actionEditerActivite = function() {
		$http.get("/appliWebAlpha/ServletVoyage?op=editerActivite" +
				"&idActivite=" + $scope.idActivite + 
				"&idVoyage=" + $scope.idVoyage).then(function(response) {
					$window.location.reload();
				});
	};
});