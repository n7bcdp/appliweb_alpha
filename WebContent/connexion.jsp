<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<!-- Adaptabilité du support -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Bibliothèque Angular -->
	<script src="bower_components/angular/angular.min.js"></script>

	<!-- Références vers les pages de style et de script -->
	<link rel="stylesheet" type="text/css" href="style/main.css">
	<script src="js/main.js"></script>

	<!-- Titre -->
	<title> PlanMyTrip </title>
</head>


<body ng-app="myApp">

	<!-- Container Principal -->
	<div id="container">
		
		<!-- Titre -->
		<div id="title">
			<div class="title-text"> Connexion </div>
			<a href="index.html" class="button"> Retour </a>
		</div>
		
		<!-- Contenu -->
		<div id="content">
			<form method="post" action="ServletCompte">
				<table class="form connexion">
					<tr> <th> Adresse électronique </th> </tr>
					<tr> <th> <input type="text" name="registerEMail"> </th> </tr>
					<tr> <th> Mot de passe </th> </tr>
					<tr> <th> <input type="password" name="registerPassword"> </th> </tr>
				</table>
				<input type="hidden" name="op" value="connexion">
				<input type="submit" class="button" value="Connecter"> <br> <br>
				<% 
				String connexionInvalide = (String) request.getAttribute("connexionInvalide"); 
				if (connexionInvalide != null) {
				%>
					<%= connexionInvalide %>
				<% } %>
			</form>
		${sessionScope.test}		
		</div>
		<!-- Fin Contenu -->
	</div>
	<!-- Fin Container -->
</body>
</html>