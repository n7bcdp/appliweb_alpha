<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<!-- Adaptabilité du support -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bibliothèque Angular -->
	<script src="bower_components/angular/angular.min.js"></script>

	<!-- Références vers les pages de style et de script -->
	<link rel="stylesheet" type="text/css" href="style/main.css">
	<script src="js/main.js"></script>

	<!-- Titre -->
	<title> PlanMyTrip </title>
</head>

<body ng-app="myApp">
	<div id="container">
		<!-- Le titre ici -->
		<div id="title">
			<div class="title-text"> Mon Compte </div>
		</div>
		<div id="content">
			<!-- Le contenu ici. -->
			<form method="post" action="ServletCompte">
				
				<% String modifPrenom = (String) request.getAttribute("modifPrenom"); %>
				<% String modifNom = (String) request.getAttribute("modifNom"); %>
				<% String modifPays = (String) request.getAttribute("modifPays"); %>
				<% String modifPassword = (String) request.getAttribute("modifPassword"); %>
				<% String modifAdresse = (String) request.getAttribute("modifAdresse"); %>
				
				<table class="form">
					<tr>
						<th><label> Nouveau Nom </label></th>
						<th><input type="text" name="modifnom" placeholder=<%= modifNom %>></th>
					</tr>

					<tr>
						<th><label> Nouveau Prenom </label></th>
						<th><input type="text" name="modifprenom" placeholder=<%= modifPrenom %>></th>
					</tr>
					
					<tr>
						<th><label> Nouveau Pays </label></th>
						<th><input type="text" name="modifpays" placeholder=<%= modifPays %>></th>
					</tr>
					
					<tr>
						<th><label> Nouveau Mot de Passe </label></th>
						<th><input type="text" name="modifpassword" placeholder=<%= modifPassword %>></th>
					</tr>
					
					<tr>
						<th><label> Nouvelle Adresse </label></th>
						<th><input type="text" name="modifadresse" placeholder=<%= modifAdresse %>></th>
					</tr>
				</table>
				
				<input type="submit" class="button" value="Appliquer">
				<input type="hidden" name="idUser" value="1">
				<input type="hidden" name="op" value="Modification">
				
				 ${sessionScope.test}
			</form>
		</div>
	</div>

</body>
</html>