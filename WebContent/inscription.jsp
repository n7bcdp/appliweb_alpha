<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<!-- Adaptabilité du support -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bibliothèque Angular -->
	<script src="bower_components/angular/angular.min.js"></script>

	<!-- Références vers les pages de style et de script -->
	<link rel="stylesheet" type="text/css" href="style/main.css">
	<script src="js/main.js"></script>
	<script src="js/Formulaire.js"></script>

	<!-- Titre -->
	<title> PlanMyTrip </title>
</head>

<body ng-app="myApp">

	<div ng-include="'includes/top.html'"></div>

	<div id="container">
		<div id="title">
			<div class="title-text">Inscription</div>
			<!-- Le titre ici -->
			<a href="index.html" class="button"> Retour </a>
		</div>
		<div id="content">
			<!-- Le contenu ici. -->
			<form method="post" action="ServletCompte" onsubmit="return verifForm(this)">

				<table class="form">
					<tr>
						<th>Nom</th>
						<th><input type="text" name="registerFirstName" onblur="verifNom(this)" ></th>
						<div id="monTest"></div>
					</tr>

					<tr>
						<th>Prénom</th>
						<th><input type="text" name="registerLastName"></th>
					</tr>

					<tr>
						<th>Adresse électronique *</th>
						<th><input type="text" name="registerEMail" onblur="verifMail(this)" ></th>
					</tr>

					<tr>
						<th>Mot de passe *</th>
						<th><input type="password" name="registerPassword" onblur = "verifMDP(this)"></th>
					</tr>
					<tr>
						<th>Confirmation du mot de passe *</th>
						<th><input type="password" name="registerPasswordValidation"></th>
					</tr>
					<tr>
						<th>Pays</th>
						<th><input type="text" name="registerCountry"></th>
					</tr>
					<tr>
						<th>Numéro de téléphone</th>
						<th><input type="text" name="registerPhone"></th>
					</tr>


				</table>
				<input type="hidden" name="op" value="inscription">
				<input type="submit" class="button" value="S'inscrire"> 
					<br>
					<% String mailInvalide = (String) request.getAttribute("mailInvalide"); %>
					<% if (mailInvalide!= null) { %>
						<%=mailInvalide %>
					<% }%>
					<br>
					<% String mdpNonIdentique = (String) request.getAttribute("mdpNonIdentique"); %>
					<% if (mdpNonIdentique!= null) { %>
						<%=mdpNonIdentique %>
					<% }%>
					<br>
					<% String mdpProbleme = (String) request.getAttribute("mdpProbleme"); %>
					<% if (mdpProbleme != null) { %>
						<%= mdpProbleme %>	
					<% }%>
				
			</form>
			<h5 id="commentaire">Les champs marqués par (*) sont nécessaires
				à la création d'un compte.</h5>
		</div>
	</div>


	<div ng-include="'includes/footer.html'"></div>
</body>
</html>